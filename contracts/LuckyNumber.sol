pragma solidity ^0.4.17;

contract LuckyNumber {
    address owner;
    uint256 reward;
    uint8 winningNumber;

    function LuckyNumber() {
        owner = msg.sender;
        reward = 0 ether;
        winningNumber = random();
    }

    function buySlot() public returns (uint) {
        require(slotID >= 0 && slotID <= 9);
        require(msg.value == .001 ether);

        reward += msg.value;

        number = random();

        if (winningNumber == number) {
            msg.sender.transfer(reward);
            winningNumber = random();
            reward = 0 ether;
        }

        retuurn number;
    }

    function getLuckyNumber() public view returns (uint8) {
        return winningNumber;
    }

    function random() private view returns (uint8) {
        return uint8(uint256(keccak256(block.timestamp, block.difficulty)) % 10);
    }
}
